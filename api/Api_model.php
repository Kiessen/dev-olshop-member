<?php

class Api_model extends CI_Model
{
	function general_select($table, $column = NULL, $where = NULL, $where_value = NULL, $order_column = NULL, $order_sort = NULL, $limit = NULL)
	{
		if ($column != NULL) {
			$this->db->select($column);
		}

		else {
			$this->db->select('*');
		}

		if ($where != NULL && $where_value != NULL) {
			$this->db->where($where, $where_value);
		}

		if ($order_column != NULL && $order_sort != NULL) {
			$this->db->order_by($order_column, $order_sort);
		}

		if ($limit != NULL) {
			$this->db->limit($limit);
		}

		$this->db->from($table);
		$query = $this->db->get();

		return $query;
	}

	function join_3_tables_where_det($id_order)
	{
		$this->db->select('*');
    	$this->db->from('tb_order');
    	$this->db->join('tb_det_order', 'tb_order.id_order = tb_det_order.id_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer = tb_costumer.id_costumer');
        $this->db->join('tb_konf_bayar', 'tb_order.id_order = tb_konf_bayar.id_order');
    	$this->db->where('tb_order.id_order', $id_order);
        $query = $this->db->get();
        return $query;
	}

	function join_3_tables_where_byr($select, $id_order,$id_costumer)
	{
		$this->db->select($select);
    	$this->db->from('tb_order');
    	$this->db->join('tb_det_order', 'tb_order.id_order = tb_det_order.id_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer = tb_costumer.id_costumer');
        $this->db->join('tb_resto', 'tb_costumer.id_costumer = tb_resto.id_resto');
        $this->db->join('tb_konf_bayar', 'tb_order.id_order = tb_konf_bayar.id_order');
    	$this->db->where('tb_order.id_order', $id_order);
        $query = $this->db->get();
        return $query;
	}

	function ambil_data($data)
	{
		$user = $this->db->get_where('tb_owner',$data);
	 	return $user->num_rows();
	}

	function  ambil_data_customer($data)
	{
		$user = $this->db->get_where('tb_costumer',$data);
	 	return $user->num_rows();

	 	if($user->num_rows()>0){

	 	}else {
	 		return 0;
	 	}

	}


	function select($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();

		return $query;
	}

function tampilan_semua_data()
 {
  //mengambil nilai atau data dari database
        return $this->db->get('tb_alamat');
 }

 function simpan_data($simpanlokasi) //fungsi simpan data
 {
  //memasukkan ke database
  return $this->db->insert('tb_alamat', $simpanlokasi);
 }

	function select_where($table, $column, $where, $where_value)
	{
		$this->db->select($column);
		$this->db->where($where, $where_value);
		$this->db->from($table);
		$query = $this->db->get();

		return $query;
	}

	function select_login($table, $column, $where, $where_value, $where2, $where_value2)
	{
		$this->db->select($column);
		$this->db->where($where, $where_value);
		$this->db->where($where2, $where_value2);
		$this->db->from($table);
		$query = $this->db->get();
		return $query;
	}

		function select_owner($table, $column, $where_value)
	{
		$this->db->select($column);
		$this->db->where('id_owner', $where_value);
		$this->db->from($table);
		$query = $this->db->get();

		return $query;
	}

	// Select single data
	function select_where_2($table, $column, $where, $where_value, $where2, $where_value2)
	{
		$this->db->select($column);
		$this->db->where($where, $where_value);
		$this->db->where($where2, $where_value2);
		$this->db->from($table);
		$query = $this->db->get();

		return $query;
	}

	function select_sum_where($table, $column_sum, $where, $where_value)
	{
		$this->db->select_sum($column_sum);
		$this->db->where($where, $where_value);
		$query = $this->db->get($table);
		return $query;
	}

	function select_sum_total($table, $where, $where_value)
	{
		$this->db->select('sum(sub_total) as total');
		$this->db->where($where, $where_value);
		$query = $this->db->get($table);
		return $query;
	}

	function select_order_by($table, $order_column, $order_sort)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($order_column, $order_sort);
		$query = $this->db->get();

		return $query;
	}

	// // Select count of data in table
	// function num_rows($table, $column, $where, $where_value)
	// {
	// 	$this->db->select($column);
	// 	$this->db->where($where, $where_value);
	// 	$this->db->from($table);
	// 	$query = $this->db->get();

	// 	return $query->num_rows();
	// }
	// // Select count of data in 2 table
	// function num_rows2($table, $column, $where, $where_value, $where2, $where_value2)
	// {
	// 	$this->db->select($column);
	// 	$this->db->where($where, $where_value);
	// 	$this->db->where($where2, $where_value2);
	// 	$this->db->from($table);
	// 	$query = $this->db->get();

	// 	return $query->num_rows();
	// }
	function select_barang($id)
	{
		$data['res']=$this->db->query("SELECT * FROM tb_barang a, tb_category b WHERE a.id_category=b.id_category");
		return $data;
	}

	function get_kode_menu()
	{
  		//$tanggal = date("now");
  		$kode = 'MN';
  		$query = $this->db->query("SELECT MAX(id_menu) as max_id FROM tb_menu");
  		$row = $query->row_array();
  		$max_id = $row['max_id'];
  		$max_id1 =(int) substr($max_id,7,4);
  		$kode_menu = $max_id1 +1;
  		$maxkode_menu = $kode.'-'.sprintf("%04s",$kode_menu);
  		return $maxkode_menu;
 	}

    function update_pesanan($id_order, $cara_pembayran) {

        $this->db->where('id_order', $id_order);
        return $this->db->update('tb_order', array('cara_pembayaran' => $cara_pembayaran ));
    }

    function update_pass($id_costumer, $data)
    {

        $this->db->where('id_costumer', $id_costumer);
        return $this->db->update('tb_costumer', $data);

    }

    function update_ket($id_order, $data)
    {

        $this->db->where('id_order', $id_order);
        return $this->db->update('tb_order', $data);

    }

    function update_status($id_order, $data)
    {

        $this->db->where('id_order', $id_order);
        return $this->db->update('tb_order', $data);

    }

    function update_owner($id_owner, $id_resto, $data, $data2)
    {
    	$this->db->where('id_resto', $id_resto);
        $status =  $this->db->update('tb_owner', $data);
    	if($status){
        $this->db->where('id_resto', $id_resto);
    	$status2 =  $this->db->update('tb_resto', $data2);
    	if($status2)
   			return true;
				else
    		return false;
   		}
				else
    		return false;
    }

   	function getid($id)
   	{
        return $this->db->get_where('tb_barang', array('id' => $id))->row();
    }

    function getidorder($id)
   	{
        return $this->db->get_where('tb_order', array('id_order' => $id))->row();
    }

	function getidowner($id_owner)
   	{
        return $this->db->get_where('tb_owner', array('id_owner' => $id_owner))->row();
    }

    function getidresto($id_resto)
   	{
        return $this->db->get_where('tb_resto', array('id_resto' => $id_resto))->row();
    }
  	function getidcostumer($id_costumer)
   	{
        return $this->db->get_where('tb_costumer', array('id_costumer' => $id_costumer))->row();
    }
    function getidcategory($id_category)
   	{
        return $this->db->get_where('tb_category', array('id_category' => $id_category))->row();
    }


    function get_id($id_transaksi) {
        return $this->db->get_where('tb_transaksi', array('id_transaksi' => $id_transaksi))->row();
    }


	function update_data($where,$data,$table)
	{
		$this->db->where($where);
		$result = $this->db->update($table,$data);
		if($result)
			return true;
		else
			return false;
	}

	function update($table, $id, $data)
	{
		$this->db->where('id_owner', $id);

		$result=$this->db->update($table, $data);
	    if($result)
		    return true;
	    else
		    return false;
	}

		function updatecs($table, $id, $data)
	{
		$this->db->where('id_costumer', $id);
		$result=$this->db->update($table, $data);
	if($result)
		return true;
	else
		return false;
	}


	function updatenya($table, $id, $data)
	{
		$this->db->where('id_order', $id);
		$this->db->update($table, $data);
	}

	function update_column($table, $where, $where_value, $data)
	{
		$this->db->where($where, $where_value);
		$this->db->update($table, $data);
	}

	// function login_admin($username, $password)
	// {
	// 	$this->db->select('*');
	// 	$this->db->where('username', $username);
	// 	$this->db->where('password', $password);
	// 	$query = $this->db->get('user');

	// 	if($query->num_rows() == 0)
	// 	{
	// 		return false;
	// 	}
	// 	else
	// 	{
	// 		return true;
	// 	}
	// }

	function get_admin_session($username, $password)
	{
		$this->db->select('*');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('user');
		$admin_data = $query->row();
		return $admin_data;
	}

	function join_2($first_table, $second_table, $select, $col_first, $col_second,$where, $where_value)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
		$this->db->where($where, $where_value);
		$query = $this->db->get();
		return $query;
	}

	function join_2_promo($first_table, $second_table, $select, $col_first, $col_second,$where, $where_value,$where2, $where_value2,$limit)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
		$this->db->where($where2, $where_value2);
		$this->db->where($where, $where_value);
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query;
	}

	function join_2_costumer($where_value)
	{
		$this->db->select('*');
		$this->db->from('tb_order');
		$this->db->join('tb_costumer', 'tb_costumer'.'.'.'id_costumer' .'='. 'tb_order'.'.'.'id_costumer');
		$this->db->where('tb_order.id_order', $where_value);
		$query = $this->db->get();
		return $query;
	}


	function join($first_table, $second_table, $select, $col)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col.'='. $first_table.'.'.$col);
		$query = $this->db->get();
		return $query;
	}

	function join_order_by($first_table, $second_table, $select, $col_first, $col_second, $order_column, $order_by)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
		$this->db->order_by($order_column, $order_by);
		$query = $this->db->get();
		return $query;
	}

	function join_where($first_table, $second_table, $select, $col_first, $col_second, $where, $where_value)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->where($where, $where_value);
		$query = $this->db->get();
		return $query;
	}

	function join_where2($first_table, $second_table, $select, $col_first, $col_second, $where, $where_value, $where2, $where_value2)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->where($where, $where_value);
        $this->db->where($where2, $where_value2);
		$query = $this->db->get();
		return $query;
	}

	function join_where23($first_table, $second_table, $select, $col_first, $col_second, $where, $where_value, $where2, $where_value2, $where3, $where_value3)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->where($where, $where_value);
        $this->db->where($where2, $where_value2);
        $this->db->where($where3, $where_value3);
		$query = $this->db->get();
		return $query;
	}

	function join_where_limit($first_table, $second_table, $select, $col_first, $col_second, $where, $where_value, $limit)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->where($where, $where_value);
		$this->db->limit($limit);
		$query = $this->db->get();
		return $query;
	}

	function join_where_limit_order_by($first_table, $second_table, $select, $col_first, $col_second, $where, $where_value, $limit, $order_column, $order_sort)
	{
		$this->db->select($select);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->where($where, $where_value);
		$this->db->limit($limit);
		$this->db->order_by($order_column, $order_sort);
		$query = $this->db->get();
		return $query;
	}

	function join_3_tables_where($id_order)
	{
		$this->db->select('*');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer = tb_costumer.id_costumer');
    	$this->db->join('tb_resto', 'tb_resto.id_resto = tb_costumer.id_resto');
        $this->db->where('tb_order.id_order', $id_order);
        $query = $this->db->get();
        return $query;
	}

    function join_3_tables($tgl_transaksi)
	{
		$this->db->select('*');
    	$this->db->from('tb_konfirmasi');
    	$this->db->join('tb_transaksi', 'tb_konfirmasi.id_transaksi = tb_transaksi.id_transaksi');
    	$this->db->join('tb_det_transaksi', 'tb_transaksi.id_transaksi = tb_det_transaksi.id_transaksi');
    	$this->db->join('tb_barang', 'tb_det_transaksi.id = tb_barang.id');
        $this->db->where('tb_transaksi.tgl_transaksi', $tgl_transaksi);
        $query = $this->db->get();
        return $query;
	}

	function free_join_3_tables_where($select ,$first_table, $second_table, $third_table, $col_first, $col_second, $col_third, $where, $where_value)
	{
		$this->db->select($select);
        $this->db->from($first_table);
        $this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
        $this->db->join($third_table, $third_table.'.'.$col_third .'='. $first_table.'.'.$col_third, 'left');
        $this->db->where($where, $where_value);
        $query = $this->db->get();
        return$query;
	}

	function join_3_tables_menu($where, $where_value)
	{
		$this->db->select('tb_menu.menu,tb_menu.id_menu,tb_menu.gambar,tb_menu.harga,tb_menu.diskon,tb_menu.date,tb_menu.id_category,tb_menu.id_resto,tb_menu.ket, ROUND( if( diskon >0, (tb_menu.harga - ( tb_menu.diskon * 0.01 * tb_menu.harga ) ) , tb_menu.harga )) AS harga_diskon');
    	$this->db->from('tb_menu');
    	$this->db->join('tb_resto', 'tb_menu.id_resto= tb_resto.id_resto');
    	$this->db->join('tb_category', 'tb_category.id_category = tb_menu.id_category');
    	$this->db->where('tb_menu.id_resto', $where);
    	$this->db->where('tb_menu.id_category', $where_value);
    	$this->db->order_by('date','DESC');
        $query = $this->db->get();
        return $query;
	}


	function join_3_tables_pesanan($where_value)
	{
		$where_value2 = array('2','3');
		$this->db->select('tb_order.id_order, tb_order.tgl_order, tb_order.total, tb_order.status, tb_order.ongkir');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->join('tb_resto', 'tb_costumer.id_resto= tb_resto.id_resto');
    	$this->db->where('tb_resto.id_resto', $where_value);
    	$this->db->where_in('status', $where_value2);
    	$this->db->order_by('tgl_order','DESC');
        $query = $this->db->get();
        return $query;
	}


	function join_2_tables_pesanan($where_value)
	{
		$this->db->select('*');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->join('tb_konf_bayar', 'tb_konf_bayar.id_order= tb_order.id_order');
    	$this->db->where('tb_order.id_order', $where_value);
        $query = $this->db->get();
        return $query;
	}



	function join_2_tables_notif($where_value)
	{
		$this->db->select('tb_owner.fcm_id as token');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->join('tb_resto', 'tb_costumer.id_resto= tb_resto.id_resto');
    	$this->db->join('tb_owner', 'tb_owner.id_resto= tb_resto.id_resto');
    	$this->db->where('tb_order.id_order', $where_value);
        $query = $this->db->get();
        return $query;
	}
	function join_3_tables_pesanan_cost($where_value)
	{
		$where_value2 = array('1','2');
		$this->db->select('tb_order.id_order, tgl_order, total, status, ongkir');
    	$this->db->from('tb_order');
    	$this->db->where('id_costumer', $where_value);
    	$this->db->where_in('status',$where_value2);
    	$this->db->order_by('tgl_order','DESC');
        $query = $this->db->get();
        return $query;
	}

	function join_3_tables_pesan($where_value)
	{
		$this->db->select('tb_order.id_order, tb_order.tgl_order, tb_order.total');
    	$this->db->from('tb_det_order');
    	$this->db->join('tb_order', 'tb_det_order.id_order= tb_order.id_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->join('tb_resto', 'tb_costumer.id_resto= tb_resto.id_resto');
    	$this->db->where('tb_resto.id_resto', $where_value);
    	$this->db->order_by('tgl_order','DESC');
        $query = $this->db->get();
        return $query;
	}

	function pesanan_costumer($where_value)
	{
		$this->db->select('menu,qty,sub_total,tb_det_order.id_menu,gambar');
    	$this->db->from('tb_det_order');
    	$this->db->join('tb_order', 'tb_det_order.id_order= tb_order.id_order');
    	$this->db->join('tb_menu', 'tb_det_order.id_menu= tb_menu.id_menu');
    	$this->db->where('tb_order.status', '0');
    	$this->db->where('tb_order.id_costumer', $where_value);
        $query = $this->db->get();
        return $query;
	}



	function ket_pesanan_costumer($where_value)
	{
		$this->db->select('ket, tb_order.id_order, tgl_order, nama, ongkir, total');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->where('tb_order.id_costumer', $where_value);
    	$this->db->where('tb_order.status', '0');

        $query = $this->db->get();
        return $query;

	}

		function det_pesanan_costumer($where_value)
	{
		$this->db->select('menu,qty,sub_total,tb_det_order.id_menu,gambar');
    	$this->db->from('tb_det_order');
    	$this->db->join('tb_order', 'tb_det_order.id_order= tb_order.id_order');
    	$this->db->join('tb_menu', 'tb_det_order.id_menu= tb_menu.id_menu');
    	$this->db->where('tb_order.id_order', $where_value);
        $query = $this->db->get();
        return $query;
	}

		function ketdet_pesanan_costumer($where_value)
	{
		$this->db->select('ket, tb_order.id_order, tgl_order, nama, ongkir, total,status');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->where('tb_order.id_order', $where_value);

        $query = $this->db->get();
        return $query;

	}

    function select_left_join($table, $coloumn, $table2, $where, $where_value){
        $this->db->select($coloumn);
        $this->db->from($table);
        $this->db->join($table2, $where = $where_value,'left');
        $query = $this->db->get();
    }

		function select_order_costumer($where_value)
	{
		$this->db->select('tb_order.*, tb_costumer.*');
    	$this->db->from('tb_order');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->where('tb_order.id_order', $where_value);

        $query = $this->db->get();
        return $query;

	}

	function join_3_tables_detpesanan($where)
	{
		$this->db->select('tb_menu.id_menu, tb_menu.menu, tb_det_order.qty, (tb_menu.harga - (tb_menu.diskon * 0.01 * tb_menu.harga )) AS harga');
    	$this->db->from('tb_det_order');
    	$this->db->join('tb_order', 'tb_det_order.id_order= tb_order.id_order');
    	$this->db->join('tb_menu', 'tb_menu.id_menu= tb_det_order.id_menu');
    	$this->db->join('tb_costumer', 'tb_order.id_costumer= tb_costumer.id_costumer');
    	$this->db->join('tb_resto', 'tb_costumer.id_resto=tb_resto.id_resto');
    	$this->db->where('tb_det_order.id_order', $where);
    	$this->db->order_by('tgl_order','DESC');
        $query = $this->db->get();
        return $query;
	}
	function select_limit($table, $limit)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->limit($limit);
		$query = $this->db->get();

		return $query;
	}

	function select_limit_order_by($table, $limit, $order_column, $order_by)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->limit($limit);
		$this->db->order_by($order_column, $order_by);
		$query = $this->db->get();

		return $query;
	}

	function select_limit_order_by_where($table, $limit, $order_column, $order_by, $where, $where_value)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where, $where_value);
		$this->db->limit($limit);
		$this->db->order_by($order_column, $order_by);
		$query = $this->db->get();

		return $query;
	}

	function get_pagination($table, $column, $where, $where_value, $order_column, $order_sort, $limit_start, $limit_stop)
    {
    	$this->db->select($column);
		$this->db->from($table);
		if($where!=NULL || $where_value!=NULL)
		{
			$this->db->where($where, $where_value);
		}
		$this->db->order_by($order_column, $order_sort);
		$this->db->limit($limit_start, $limit_stop);
		$query = $this->db->get();

		return $query;
    }

	function get_pagination_join($first_table, $second_table, $column, $col_first, $col_second, $where, $where_value, $order_column, $order_sort, $limit_start, $limit_stop)
    {
    	$this->db->select($column);
		$this->db->from($first_table);
		$this->db->join($second_table, $second_table.'.'.$col_second .'='. $first_table.'.'.$col_first);
		if($where!=NULL || $where_value!=NULL)
		{
			$this->db->where($where, $where_value);
		}
		$this->db->order_by($order_column, $order_sort);
		$this->db->limit($limit_start, $limit_stop);
		$query = $this->db->get();

		return $query;
    }
//-----admin-----------
    function input_data($data,$table){
		if($this->db->insert($table,$data))
			return true;
		else
			return false;
	}


//--------------------
	function insert_entry($data_order, $data_det_order)
	{
    $this->db->insert('tb_order', $data_order);
    //$data_det_order['id_order'] = $this->db->insert_id();
    $this->db->insert('tb_det_order', $data_det_order);
	}

	function insert_last_id($table, $data)
	{
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}

	function delete($table, $where, $where_value)
	{
		$this->db->where($where, $where_value);
		$this->db->delete($table);
	}

	function hapus_data($where, $table){
		$this->db->where($where);
		$status=$this->db->delete($table);
        if($status)
   			return true;
				else
    		return false;
	}

    function hapus_order($id_order)
    {
        $query=$this->db->query("DELETE FROM `tb_order` WHERE `tb_order`.`id_order` = '$id_order'");
        if ($query)
        	return true;
        else
        	return false;
	}
	function hapus_det_order($id_order)
    {
        $query=$this->db->query("DELETE FROM `tb_det_order` WHERE `tb_det_order`.`id_order` = '$id_order'");
     if ($query)
        	return true;
        else
        	return false;
	}



	function buat_kode_member()
	{
		$this->db->select('RIGHT(tb_costumer.id_costumer,4) as kode', FALSE);
		$this->db->order_by('id_costumer','DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_costumer');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->kode) + 1;
			}
				else
			{
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "CS-".$kodemax;
		return $kodejadi;
	}

	function buat_kode_konf()
	{
		$this->db->select('RIGHT(tb_konf_bayar.id_konf_bayar,4) as kode', FALSE);
		$this->db->order_by('id_konf_bayar','DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_konf_bayar');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->kode) + 1;
			}
				else
			{
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "CB"."-".$kodemax;
		return $kodejadi;
	}



	function buat_kode_order()
	{
		$this->db->select('RIGHT(tb_order.id_order,4) as kode', FALSE);
		$this->db->order_by('id_order','DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_order');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->kode) + 1;
			}
				else
			{
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "TR-".$kodemax;
		return $kodejadi;
	}

	function buat_kode_menu()
	{
		$this->db->select('RIGHT(tb_menu.id_menu,4) as kode', FALSE);
		$this->db->order_by('id_menu','DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_menu');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->kode) + 1;
			}
				else
			{
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "MN"."-".$kodemax;
		return $kodejadi;
	}

	function remove_checked_kategori()
	{
		// $action = $this->input->post('action');
		// if ($action == "delete") {
			$delete = $this->input->post('msg');
			for ($i=0; $i < count($delete) ; $i++) {
				$this->db->where('id_category', $delete[$i]);
				$this->db->delete('tb_category');
			// }
		}
	}

	function cari_menu($id, $id_category)
	{
		$results=$this->db->query("SELECT *, ROUND( if( diskon >0, (harga - ( diskon * 0.01 * harga ) ) , harga )) AS harga_diskon FROM tb_menu WHERE menu LIKE '%$id%' and id_category='$id_category';");
		return $results;
	}

    function barang($id_owner)
    {
        $results = $this->db->query("select b.*, ifnull(sum(d.qty),0) as terjual from tb_barang b left join tb_det_transaksi d on b.id = d.id where b.stok > 0 and diskon=0 and b.id_owner='$id_owner' group by b.id order by date desc");
        return $results;
	}

	function cari_kategori($id_category)
	{
		$results=$this->db->query("SELECT * FROM tb_category WHERE id_category = '%$id_category%' OR category LIKE '%$id_category%';");
		return $results;
	}

    function cari_laporan($tgl_transaksi1,$tgl_transaksi2)
	{

		$results=$this->db->query("SELECT *, ((harga_barang-(harga_barang*diskon/100))*qty) as harbar FROM tb_konfirmasi a, tb_transaksi b, tb_det_transaksi c, tb_barang d where a.id_transaksi=b.id_transaksi and b.id_transaksi=c.id_transaksi and c.id=d.id and tgl_transaksi between '$tgl_transaksi1' and '$tgl_transaksi2';");
		return $results;
	}

    function jumlah_laporan($tgl_transaksi1,$tgl_transaksi2)
	{

		$results=$this->db->query("SELECT sum((harga_barang-(harga_barang*diskon/100))*qty) as jumlah, avg((harga_barang-(harga_barang*diskon/100))*qty) as rata FROM tb_konfirmasi a, tb_transaksi b, tb_det_transaksi c, tb_barang d where a.id_transaksi=b.id_transaksi and b.id_transaksi=c.id_transaksi and c.id=d.id and tgl_transaksi between '$tgl_transaksi1' and '$tgl_transaksi2'");
		return $results;
	}



	function insert_media($data)
    {
        $this->db->insert('tb_category', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }


    function rekap()
    {
        $results=$this->db->query("SELECT SUM(total) as total, monthname(tgl_transaksi) as bulan FROM tb_transaksi GROUP BY MONTH(tgl_transaksi)");
        return $results;
    }

	function select_idorder($id_costumer,$status)
    {
        $query=$this->db->query("Select max(id_order) from tb_order where id_costumer = '$id_costumer' and status = '$status' ORDER BY id_order DESC");
        return $query;
    }



	function barang_diskon_admin($id_owner)
    {
        $query=$this->db->query("select b.*, (b.harga_barang - ( b.diskon * 0.01 * b.harga_barang )) AS harga_diskon, ifnull(sum(d.qty),0) as terjual from tb_barang b left join tb_det_transaksi d on b.id = d.id where b.diskon > 0 and b.id_owner='$id_owner' group by b.id order by b.diskon DESC");
        return $query;
    }

	function barang_populer_admin($id_owner)
    {
        $query=$this->db->query("select b.*,ifnull(sum(d.qty),0) as terjual from tb_barang b left join tb_det_transaksi d on b.id = d.id where b.diskon = 0  and (select ifnull(sum(d.qty),0) as terjual)>0 and b.id_owner='$id_owner' group by b.id order by terjual desc limit 0,20");
        return $query;
    }

	function send_notif($id_order)
    {
        $query=$this->db->query("SELECT tb_owner.fcm_id as token FROM `tb_order` join tb_costumer on tb_order.id_costumer=tb_costumer.id_costumer join tb_owner on tb_owner.id_resto=tb_costumer.id_resto where tb_order.id_order='$id_order'");
       return $query->result_array();
    }

    function send_notif2($id_order)
    {
        $query=$this->db->query("SELECT tb_costumer.fcm_id as token FROM `tb_order` join tb_costumer on tb_order.id_costumer=tb_costumer.id_costumer where tb_order.id_order='$id_order' and tb_costumer.fcm_id is not null");
       return $query->result_array();
    }


    public function get_idorder($id_costumer,$status)
    {
    $this->db->select_max('id_order');
    $this->db->where('id_costumer', $id_costumer);
    $this->db->where('status', $status);
    $query = $this->db->get('tb_order');
    $data = $query->row();
		return $data;

	}
	public function barang_terbaru_member($id_owner){
		$res=$this->db->query("SELECT
			b . * ,
			ROUND( if( diskon >0, (b.harga - ( b.diskon * 0.01 * b.harga ) ) , '0' )) AS harga_diskon,
			ifnull( sum( d.qty ) , 0 ) AS terjual,
			ifnull(sum(t.rate)/count(t.rate) , 0) AS rating
				FROM
					tb_barang b
			LEFT JOIN tb_det_transaksi d ON b.id = d.id
			LEFT JOIN tempat_komentar t ON b.id = t.komentar_id
			WHERE
				b.stok >0
				and
				b.id_owner='$id_owner'
			GROUP BY
				b.id
			ORDER BY
				date DESC");
		return $res;
	}
//------------------- im
	public function barang_populer_member($id_owner){
		$res=$this->db->query("select b.*, ROUND(if(diskon >0, (b.harga - (
			b.diskon * 0.01 * b.harga )), '0')) AS harga_diskon ,ifnull(sum(d.qty),0) as terjual ,
			ifnull( sum( t.rate ) / count( t.rate ) , 0 ) AS rating  from tb_barang b left join
			tb_det_transaksi d on b.id = d.id
			LEFT JOIN tempat_komentar t ON b.id = t.id where (select ifnull(sum(d.qty),0) as terjual)>0 and stok>0
			and b.id_owner='$id_owner' group by b.id order by terjual desc");

		return $res;
	}

	public function barang_diskon_member($id_owner){
		$res=$this->db->query("select b.*, (b.harga - ( b.diskon * 0.01 * b.harga )) AS harga_diskon,
		ifnull(sum(d.qty),0) as terjual  , ifnull(sum(t.rate)/count(t.rate) , 0) AS rating
		from tb_barang b left join tb_det_transaksi d on b.id = d.id  LEFT JOIN tempat_komentar t ON b.id = t.id
		 where  b.stok > 0 and b.diskon > 0 and b.id_owner='$id_owner' group by b.id order by b.diskon DESC");

		 return $res;
	}
	// ----------------------------------------

	public function barang_cat($id_owner){
		$res=$this->db->query("SELECT b.*, ROUND(if(diskon >0, (b.harga - ( b.diskon * 0.01 * b.harga )), '0')) as harga_diskon, ifnull(sum(d.qty),0) as terjual, ifnull(sum(t.rate)/count(t.rate) , 0) AS rating  FROM tb_barang b left join
		tb_det_transaksi d on b.id=d.id LEFT JOIN tempat_komentar t ON b.id = t.id where id_category ='$category' and b.stok >0 and id_owner='$id_owner' group by id");

			return $res;
	}

	function select_like($table, $title, $match)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($title, $match);
		$query = $this->db->get();

		return $query;
	}
	function buat_kode_cat()
	{
	    //SELECT max(id_category) as terakhir from tb_category
		$this->db->select('RIGHT(tb_category.id_category,4) as kode', FALSE);
		$this->db->order_by('id_category','DESC');
		$this->db->limit(1);
		$query = $this->db->get('tb_category');
		if($query->num_rows() <> 0){
			$data = $query->row();
			$kode = intval($data->kode) + 1;
			}
				else
			{
			$kode = 1;
		}
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kodejadi = "IC_".$kodemax;
		return $kodejadi;
	}

	function select_order_by_where($table, $order_column, $order_sort, $where, $where_value)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where, $where_value);
			$this->db->order_by($order_column, $order_sort);
			$query = $this->db->get();

			return $query;
		}

		function daftar_owner($id_owner){
			$data=$this->db->query("SELECT id_owner,nama,nama_toko ,email,no_telp,bbm,wa, alamat,  deskripsi, no_rek FROM tb_owner where id_owner='$id_owner'");

			return $data;
		}





}

?>
