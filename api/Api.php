<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Api_model');
        $this->load->library('session');

    }

//-----------------owner--------------------
    function login_owner($id_owner)
    {
      $email_owner = $_POST['email_owner'];
      $password = $this->input->post('password');

      $data = array(
                'email_owner' => $email_owner,
                'password' => md5($password)
                );

      $cek = $this->Api_model->ambil_data($data);
      $cek2 = $this->Api_model->select_where_2('tb_owner','id_owner, nama_owner, email_owner, password, status','email_owner',$email_owner, 'id_owner', $id_owner)->result_array();

      if($cek == 1)
      {
        $json_response["login"] = $cek2;
        $json_response["success"] = "1";
        //array_push($json_response,$json_response);
        echo json_encode($json_response);
      }
        else
        {
        $json_response = array();
        $status["status"] = "gagal";
        array_push($json_response, $status);
        echo json_encode($json_response);
      }
  	}

	function register_fcm_owner()
	{
  		$fcm_id = $this->input->post('fcm_id');
  		$id_owner= $this->input->post('id_owner');

  		$data=array(
  			'fcm_id'=>$fcm_id
  			);

  		$result = $this->Api_model->update('tb_owner', $id_owner, $data);

  		if($result==true) {
        	$status["status"] ="berhasil";
    	}else {
        	$status["status"] ="gagal";
    	}
  		echo json_encode($status);
	}

	function daftar_owner($id_owner)
  	{
      $cek2 = $this->Api_model->select_where('tb_owner','id_owner,nama_owner,email_owner,telp_owner,bbm,line,wa,alamat,asal_kota,keterangan,rekening','id_owner',$id_owner)->result();

          echo json_encode($cek2);
  	}

  	function daftar_barang_terbaru_admin($id_owner)
  	{
        $data = $this->Api_model->barang($id_owner)->result_array();
        echo json_encode($data);
  	}

  	function daftar_barang_diskon_admin($id_owner)
  	{
        $data = $this->Api_model->barang_diskon_admin($id_owner)->result_array();
        echo json_encode($data);
  	}

  	function daftar_barang_populer_admin($id_owner)
  	{
        $data = $this->Api_model->barang_populer_admin($id_owner)->result_array();
        echo json_encode($data);
  	}

    //-------------------- pray------------------------
    function daftar_barang_terbaru_member($id_owner){
      $data=$this->Api_model->barang_terbaru_member($id_owner)->result_array();
        $json_response['barang']=$data;
        $json_response["success"] = "1";
      echo json_encode($json_response);
    }

    function daftar_barang_populer_member($id_owner){
      $data=$this->Api_model->barang_populer_member($id_owner)->result_array();
      $json_response['p_barang']=$data;
      $json_response["success"] = "1";
    echo json_encode($json_response);
    }

    function daftar_barang_diskon_member($id_owner){
      $data=$this->Api_model->barang_diskon_member($id_owner)->result_array();
      $json_response['d_barang']=$data;
      $json_response["success"] = "1";
    echo json_encode($json_response);
    }

    function daftar_barang_percat($id_owner){
      $data=$this->Api_model->barang_cat($id_owner)->result_array();
      $json_response['kategori']=$data;

      echo json_encode($json_response);
    }

    function get_cat($id_owner)
   {

     //delete---kalau ada id category-nya-------
     if (!empty($_POST['id_category'])){
       $id_category = $this->input->post('id_category');
       $del = $this->Api_model->delete('tb_category','id_category',$id_category);
       if ($del==true) {
         $status["status"]="berhasil";
       }else{
         $status["status"]="gagal";
       }
       echo json_encode($status);
     }
     //insert----kalau ada category-nya----------
     elseif (!empty($_POST['category'])) {
       $category = $this->input->post('category');
       $data = $this->Api_model->select_like('tb_category','category',$category)->num_rows();
       if ($data>0) {
         $status["status"]="ada";
       }else {
         $id = $this->Api_model->buat_kode_cat();
         $data = array(
             'id_category'=>$id,
             'category'=>$category,
             'id_owner'=>$id_owner);
         $masukan = $this->Api_model->input_data($data,'tb_category');
         if ($masukan==true) {
           $status["status"]="berhasil";
         }else{
           $status["status"]="gagal";
         }
         echo json_encode($status);
       }
     }
     //select-kalau ga ada semua cuman id_owner---
     else{
       $data = $this->Api_model->select_order_by_where('tb_category', 'category', 'asc', 'id_owner',$id_owner )->result_array();
       $json_response["category"]=$data;
       echo json_encode($json_response);
     }


   }
   public function select_daftar_owner($id_owner){
     $data=$this->Api_model->daftar_owner($id_owner)->result_array();
     $json_response['daftar_owner']=$data;

     echo json_encode($json_response);
   }


//-----------------end_owner---------------
}
?>
