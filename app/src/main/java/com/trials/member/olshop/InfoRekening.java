package com.trials.member.olshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.trials.member.server.Config;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by DELL on 25/10/2016.
 */

public class InfoRekening extends AppCompatActivity {
    TextView ttotal, trek;
    Button boke;
    String total;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_rekening);

        ttotal = (TextView) findViewById(R.id.textViewtotaltr);
        trek = (TextView) findViewById(R.id.textViewrek);
        boke = (Button) findViewById(R.id.btnSip);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Bundle b = getIntent().getExtras();
        total = b.getString("total");
        ttotal.setText("Rp. " + total);

        boke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(InfoRekening.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        norekening();
    }

    private void norekening() {
        progressBar.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Config.URL_BASE_API + "/daftar_owner_member/"+Config.ID_OWNER, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //Log.e("TAG", response.toString());
                progressBar.setVisibility(View.GONE);
                try {
                    JSONArray arr = response.getJSONArray("owner");
                    for (int i = 0; i < arr.length(); i++) {
                        //JSONObject obj = arr.getJSONObject(i);
                        JSONObject obj = arr.getJSONObject(0);
                        trek.setText(obj.getString("no_rek"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                throwable.printStackTrace();
                progressBar.setVisibility(View.GONE);
                Toast.makeText(InfoRekening.this, "Terjadi kesalahan! Coba lagi nanti", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
